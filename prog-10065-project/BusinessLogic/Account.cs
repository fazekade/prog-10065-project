﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_10065_project {
    class Account {
        #region Field variables

        /// <summary>
        /// Represents the hashed string password used to access the user's data file.
        /// </summary>
        private string _password; 
        /// <summary>
        /// Represents the username, used to access the user's data file. Case in-sensative.
        /// </summary>
        private string _userName;
        /// <summary>
        /// Represents the first name of the user.
        /// </summary>
        private string _firstName;
        /// <summary>
        /// Represents the last name of the user.
        /// </summary>
        private string _lastName;
        /// <summary>
        /// Represents the email of the user.
        /// </summary>
        private string _email;
        /// <summary>
        /// Represents the list of terms (Ex. 17F, 19W, etc.) in the user's data file.
        /// </summary>
        internal List<Term> _termList;
        /// <summary>
        /// Represents selected term (Ex. 17W) whose contents are displayed on the Dashboard and Semester pages. 
        /// </summary>
        private Term _activeTerm;
        /// <summary>
        /// Represents selected course whose contents are displayed on the Semester page.
        /// </summary>
        private Course _activeCourse;
        /// <summary>
        /// Represents the selected item.
        /// </summary>
        private Course_Item _activeItem;

        #endregion

        #region Constructor

        public Account(string password, string userName, string firstName, string lastName, string email) {
            _password    = password;
            _userName    = userName;
            _firstName   = firstName;
            _lastName    = lastName;
            _email       = email;
            _termList    = new List<Term>();
            ActiveTerm   = null;
            ActiveCourse = null;
            ActiveItem   = null;
        }

        #endregion

        #region Properties

        public string Password {
            get { return _password; }
            set { _password = value; }
        }
        public string UserName {
            get { return _userName; }
            set { _userName = value; }
        }
        public string FirstName {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public string LastName {
            get { return _lastName; }
            set { _lastName = value; }
        }
        public string Email {
            get { return _email; }
            set { _email = value; }
        }
        public Term ActiveTerm {
            get { return _activeTerm; }
            set { _activeTerm = value; }
        }
        public Course ActiveCourse {
            get { return _activeCourse; }
            set { _activeCourse = value; }
        }
        public Course_Item ActiveItem {
            get { return _activeItem; }
            set { _activeItem = value; }
        }
        //TODO: Overwrite semester toString display.

        #endregion

        #region Miscellaneous methods

        //TODO: Remove if not used.
        /// <summary>
        /// Refreshes all the computational field variables.
        /// </summary>
        internal void Refresh() {
            // Update all terms.
            foreach (Term term in _termList) {
                term.Refresh();
            }
        }

        #endregion
    }
}
