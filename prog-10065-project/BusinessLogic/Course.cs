﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_10065_project {
    class Course {
        #region Field variables

        /// <summary>
        /// Represents the string code (Ex., MATH 1080) of the course.
        /// </summary>
        private string _code;
        /// <summary>
        /// Represents the title (Ex., Intro to Calculus) of the course.
        /// </summary>
        private string _title;
        /// <summary>
        /// Represents the decimal credit (Ex., 6.00) value of the course.
        /// </summary>
        private double _credit;
        /// <summary>
        /// Represents whether the course is considered active (i.e, True). Inactive courses do not affect computations. 
        /// </summary>
        private bool _status;
        /// <summary>
        /// Represents the list of items (Ex., quizzes, tests, etc.) within the course.
        /// </summary>
        internal List<Course_Item> _items;
        /// <summary>
        /// Represents the currently achieved grade (Ex., 34.45%) in the course; the sum of weights of completed items. Read only.
        /// </summary>
        private double _achievedGrade;
        /// <summary>
        /// Represents the current average (Ex., 45.54%) in the course. Read only.
        /// </summary>
        private double _average;
        /// <summary>
        /// Represents the fractional amount of completed weights to uncompleted weights.
        /// </summary>
        private double _completion;

        #endregion

        #region Constructor

        public Course(string code, string title, double credit, bool status = true) {
            _code          = code;
            _title         = title;
            _credit        = credit;
            _status        = status;
            _items         = new List<Course_Item>();
            _achievedGrade = 0;
            _average       = 0;
            _completion    = 0;
        }

        #endregion

        #region Properties

        public string Code {
            get { return _code; }
            set { _code = value; }
        }
        public string Title {
            get { return _title; }
            set { _title = value; }
        }
        public double Credit {
            get { return _credit; }
            set { _credit = value; }
        }
        public bool Status {
            get { return _status; }
            set { _status = value; }
        }
        /// <summary>
        /// Returns the quotient of the Achieved Grade and sum of Weights of completed Items in a course. Read Only.
        /// </summary>
        public double Average {
            get {
                double completedWeights = 0;

                // Loop through all Items in the course.
                foreach (Course_Item item in _items) {
                    // Validate that the Item is completed.
                    if (item.Mark >= 0.0) {
                        completedWeights += item.Weight;
                    }
                }

                return AchievedGrade / completedWeights;
            }
        }
        /// <summary>
        /// Returns the sum of the Grades of completed items in a course. Read only.
        /// </summary>
        public double AchievedGrade {
            get {
                double achievedGrade = 0.0;

                // Loop through Items in Course. 
                foreach (Course_Item item in _items) {
                    // Validate that the Item is completed.
                    if ((item.Weight >= 0.0) && (item.Mark >= 0.0)) {
                        // Sum the Grades.
                        achievedGrade += item.Grade;
                    }
                }

                return achievedGrade;
            }
        }
        /// <summary>
        /// Returns the proprtion of completed weighted items to uncompleted ones. Read only.
        /// </summary>
        public double Completion {
            get {
                double completedWeights = 0;

                // Loop through all items in the course.
                foreach (Course_Item item in _items) {
                    // Validate that the Item is completed.
                    if ((item.Mark >= 0) && (item.Weight >= 0)) {
                        completedWeights += item.Weight;
                    }

                    // Sum all weights (should be 100%).
                    completedWeights += item.Weight;
                }

                return 1 - completedWeights;
            }
        }

        #endregion      

        #region Miscellaneous methods

        //TODO: Remove if not used.
        /// <summary>
        /// Refreshes all the computational field variables.
        /// </summary>
        internal void Refresh() {
            //_achievedGrade = Compute_AchievedGrade();
            _average = 0.0; //TODO: Replace with method call.
            _completion = 0.0; //TODO: Replace with method call.
        }

        #endregion
    }
}
