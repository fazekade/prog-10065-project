﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_10065_project {
    public class Term {
        #region Field Variables

        /// <summary>
        /// Represents the list of Courses within the Term.
        /// </summary>
        internal List<Course> _courseList;
        /// <summary>
        /// Represents the year (Ex., 2017) that the Term takes place in.
        /// </summary>
        private int _year;
        /// <summary>
        /// Represents the semester (Ex., Fall) that the Term takes place in.
        /// </summary>
        private string _semester;
        /// <summary>
        /// Represents the Grade Point Average (Ex., 4.0) of the entire Term. Read only.
        /// </summary>
        private double _gpa;
        /// <summary>
        /// Represents the average (Ex., 45.56%) of the entire Term. Read only.
        /// </summary>
        private double _average;
        /// <summary>
        /// Represents the sum of Credits (Ex., 16.00) of the entire Term. Read only.
        /// </summary>
        private double _totalCredits;
        /// <summary>
        /// Represents the sum of Credits (Ex., 8.00) of completed Courses. Read only.
        /// </summary>
        private double _earnedCredits;

        #endregion

        #region Constructors

        public Term (int year, string semester) {
            _year          = year;
            _semester      = semester;
            _courseList    = new List<Course>();
            _gpa           = 0.0;
            _average       = 0.0;
            _totalCredits  = 0.0;
            _earnedCredits = 0.0;
        }

        #endregion

        #region Properties

        public int Year
        {
            get { return _year; }
        }
        public string Semester
        {
            get { return _semester; }
        }
        public double GPA {
            //TODO: Finish computation.
            get;
        }
        public double Average {
            //TODO: Finish computation.
            get;
        }
        public double TotalCredits {
            //TODO: Finish computation.
            get;
        }
        public double CurrentCredits {
            //TODO: Finish computation.
            get;
        }
        
        #endregion

        #region Miscellaneous methods

        /// <summary>
        /// Returns a custom string representation of a Term (ex., 17F)
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            return $"{(Year.ToString()).Substring(2)}{Semester[0]}";
        }

        //TODO: Remove if not used.
        /// <summary>
        /// Refreshes all the computational field variables.
        /// </summary>
        internal void Refresh() {
            _gpa = 0.0; //TODO: Replace with method call.
            _average = 0.0; //TODO: Replace with method call.
            _totalCredits = 0.0; //TODO: Replace with method call.
            _earnedCredits = 0.0; //TODO: Replace with method call.

            // Update all courses.
            foreach (Course course in _courseList) {
                course.Refresh();
            }
        }

        #endregion
    }
}
