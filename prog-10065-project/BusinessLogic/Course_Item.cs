﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_10065_project {
    class Course_Item {
        #region Field variables

        /// <summary>
        /// Represents the category (Ex., Quiz) that the item belongs to.
        /// </summary>
        private string   _type;
        /// <summary>
        /// Represents the name (Ex., Midterm) identifying the item.
        /// </summary>
        private string   _title;
        /// <summary>
        /// Represents a portion (Ex., 14.54%) of the whole course grading schematic.
        /// </summary>
        private double   _weight;
        /// <summary>
        /// Represents the user's performance (14.54%) on the item.
        /// </summary>
        private double   _mark;
        /// <summary>
        /// Represents the user's weighted performance (i.e, product of weight and mark) on the item.
        /// </summary>
        private double   _grade;
        /// <summary>
        /// Represents the starting date of an item. 
        /// </summary>
        private DateTime _startDate;
        /// <summary>
        /// Represents the duedate of an item.
        /// </summary>
        private DateTime _dueDate;
        /// <summary>
        /// Represents whether an item was completed or not.
        /// </summary>
        private bool     _status;

        #endregion

        #region Constructor

        public Course_Item(string type, string title, double weight, DateTime due, DateTime start, double mark = -1, bool status = false) {
            _type      = type;
            _title     = title;
            _weight    = weight;
            _mark      = mark;
            _startDate = start;
            _dueDate   = due;
            _status  = status;
        }

        #endregion

        #region Properties

        public string Type {
            get { return _type; }
            set { _type = value; }
        }
        public string Title {
            get { return _title; }
            set { _title = value; }
        }
        public double Weight {
            get { return _weight; }
            set { _weight = value; }
        }
        public double Mark {
            get { return _mark; }
            set { _mark = value; }
        }
        public double Grade
        {
            get {
                return Mark * Weight;
            }
            set { _grade = value; }
        }
        public DateTime StartDate {
            get { return _startDate; }
            set { _startDate = value; }
        }
        public DateTime DueDate {
            get { return _dueDate; }
            set { _dueDate = value; }
        }
        public bool Status
        {
            get { return _status; }
            set { _status = value; }
        }

        #endregion

        #region Data-Driven Methods

        /// <summary>
        /// Updates all the properties of an Item, reassigning old values to unchanged ones, and new values to changed ones.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="title"></param>
        /// <param name="weight"></param>
        /// <param name="dueDate"></param>
        /// <param name="mark"></param>
        /// <param name="status"></param>
        internal void Update(string type = "", string title = "", double weight = -1, double mark = -1, DateTime? dueDate = null, bool status = false)
        {
            //TODO: Default mark shouldn't result in 0% Grade.
            Type    = (type == "") ? Type : type;
            Title   = (title == "") ? Title : title;
            Weight  = (weight == -1) ? Weight : weight;
            Mark    = mark;
            DueDate = (dueDate == null) ? DateTime.MaxValue : DueDate;
            Status  = status;
        }

        #endregion
    }
}
