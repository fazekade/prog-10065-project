﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Markup;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace prog_10065_project {
    public sealed partial class Semester_Marks : Page {
        #region Field variables

        /// <summary>
        /// Represents the User object, containing everything from their Terms, Courses, and Items.
        /// </summary>
        Account _user;

        SemestersPage _semesterPage;

        CalendarDatePicker _picker_dueDate;

        #endregion

        #region Constructor

        public Semester_Marks()
        {
            this.InitializeComponent();
            _semesterPage = new SemestersPage();
        }

        #endregion

        #region Navigational methods

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            TextBlock CreateTextBlock(string text, double width, double leftMargin = 0, double rightMargin = 30, string foreground = "Gray", string fontWeight = "", double fontSize = 24, string fontFamily = "Segoe UI")
            {
                // Convert foreground from string to color.
                var s_foreground = (Color)XamlBindingHelper.ConvertValue(typeof(Color), foreground);

                // Convert fontWeight from string to fontWeight. 
                var s_fontWeight = fontWeight == "Bold" ? FontWeights.Bold : FontWeights.Normal;

                // Convert fontFamily from string to fontFamily. 
                var s_fontFamily = new FontFamily(fontFamily);

                // Create and return TextBlock.
                return new TextBlock
                {
                    Text = $"{text}",
                    FontSize = fontSize,
                    Margin = new Thickness(leftMargin, 0, rightMargin, 0),
                    Foreground = new SolidColorBrush(s_foreground),
                    FontWeight = s_fontWeight,
                    Width = width,
                    VerticalAlignment = VerticalAlignment.Center,
                    FontFamily = s_fontFamily
                };
            }

            // Get User data.
            _user = e.Parameter as Account;

            // If courses exist, update page.
            if (_user.ActiveTerm._courseList.Count > 0)
            {
                #region Generate data header

                // Create stackpanel.
                StackPanel header_panel = new StackPanel()
                {
                    Orientation = Orientation.Horizontal,
                    Margin = new Thickness(10, 5, 0, 5)
                };

                // Append children to stackpanel.
                header_panel.Children.Add(CreateTextBlock("(TYPE)",    100,  leftMargin:105,  rightMargin:30));
                header_panel.Children.Add(CreateTextBlock("TITLE",     150,  fontWeight:"Bold"));
                header_panel.Children.Add(CreateTextBlock("[WEIGHT]",  110));
                header_panel.Children.Add(CreateTextBlock("GRADE",     110));
                header_panel.Children.Add(CreateTextBlock("DUEDATE",   130));

                // Create listBoxItem, and append stackpanel.
                ListBoxItem header_boxItem = new ListBoxItem { Content = header_panel, IsEnabled = false };

                // Append listBoxItem to listBox.
                _listBox_CourseList.Items.Add(header_boxItem);

                #endregion

                #region Generate data content

                // Update course listbox.
                foreach (Course_Item item in _user.ActiveCourse._items)
                {
                    // Get data from all Course Items.
                    string status  = "&#xEC92;";   //TODO: Finish.
                    string type    = item.Type.ToUpper();
                    string title   = item.Title.Length > 8 ? item.Title.Substring(0, 5) + "..." : item.Title;
                    string weight  = (item.Weight * 100).ToString() + "%";
                    string grade   = item.Mark == -1 ? "- - -" : (item.Grade * 100).ToString() + "%";
                    string dueDate = item.DueDate.ToString() == DateTime.MaxValue.ToString() ? "- - -" : $"{item.DueDate.Month}/{item.DueDate.Day}/{item.DueDate.Year}";

                    // Create stackpanel, and append course details.
                    StackPanel panel = new StackPanel()
                    {
                        Orientation = Orientation.Horizontal,
                        Margin = new Thickness(10, 5, 0, 5)
                    };
                    panel.Children.Add(CreateTextBlock(status,   75,   fontFamily:"Segoe MDL2 Assets"));
                    panel.Children.Add(CreateTextBlock($"({type})",   130));
                    panel.Children.Add(CreateTextBlock($"{title}",    150,  foreground:"Black", fontWeight:"Bold"));
                    panel.Children.Add(CreateTextBlock($"[{weight}]", 110,  foreground:"Black"));
                    panel.Children.Add(CreateTextBlock($"{grade}",    110,  foreground:"Black"));
                    panel.Children.Add(CreateTextBlock($"{dueDate}",  130,  foreground:"Black"));

                    // Create listBoxItem, and append stack panel.
                    ListBoxItem boxItem = new ListBoxItem
                    {
                        Content = panel,
                        IsDoubleTapEnabled = true
                    };

                    // Attach double-Tap event handler to listBoxItem.
                    boxItem.DoubleTapped += new DoubleTappedEventHandler(OnDoubleTap_EditItem);

                    // Generate and attach tooltip to listBoxItem
                    ToolTip toolTip = new ToolTip { Content = "DOUBLE-CLICK TO EDIT" };
                    ToolTipService.SetToolTip(boxItem, toolTip);

                    // Append listBoxItem to listBox.
                    _listBox_CourseList.Items.Add(boxItem);
                }

                #endregion
            }

            base.OnNavigatedTo(e);
        }

        #endregion

        #region Miscellaneous methods

        private async void Alert(string msg)
        {
            MessageDialog message = new MessageDialog(msg);
            await message.ShowAsync();
        }

        /// <summary>
        /// Clears the DueDate of the Add Item popup window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClick_ClearDate(object sender, RoutedEventArgs e)
        {
            _picker_dueDate.Date = null;
        }

        internal async void OnDoubleTap_EditItem(object sender, DoubleTappedRoutedEventArgs e)
        {
            //TODO: Move these methods elsewhere, make public and accessible in other classes.
            StackPanel CreateStackPanel()
            {
                return new StackPanel
                {
                    Orientation = Orientation.Horizontal,
                    Margin = new Thickness(0, 15, 0, 15)
                };
            }
            TextBlock CreateTextBlock(string text = "", double leftMargin = 0, double rightMargin = 0, string foreground = "Black", string fontWeight = "", double width = 130, double fontSize = 24)
            {
                // Convert foreground from string to color.
                var s_foreground = (Color)XamlBindingHelper.ConvertValue(typeof(Color), foreground);

                // Convert fontWeight from string to fontWeight. 
                var s_fontWeight = fontWeight == "Bold" ? FontWeights.Bold : FontWeights.Normal;

                // Create and return TextBlock.
                return new TextBlock
                {
                    Text = $"{text}",
                    FontSize = fontSize,
                    Margin = new Thickness(leftMargin, 0, rightMargin, 0),
                    Foreground = new SolidColorBrush(s_foreground),
                    FontWeight = s_fontWeight,
                    Width = width,
                    VerticalAlignment = VerticalAlignment.Center
                };
            }
            TextBox CreateTextBox(string text = "", double leftMargin = 0, double rightMargin = 0, string foreground = "Gray", string fontWeight = "", double width = 300, double fontSize = 24, string placeHolder = "")
            {
                // Convert foreground from string to color.
                var s_foreground = (Color)XamlBindingHelper.ConvertValue(typeof(Color), foreground);

                // Convert fontWeight from string to fontWeight. 
                var s_fontWeight = fontWeight == "Bold" ? FontWeights.Bold : FontWeights.Normal;

                // Create and return TextBox.
                return new TextBox
                {
                    Text = $"{text}",
                    PlaceholderText = $"{placeHolder}",
                    FontSize = fontSize,
                    Margin = new Thickness(leftMargin, 0, rightMargin, 0),
                    Foreground = new SolidColorBrush(s_foreground),
                    FontWeight = s_fontWeight,
                    Width = width,
                    VerticalAlignment = VerticalAlignment.Center
                };
            }



            //TODO: Identify selected Item.
            // Get selected Item.
            //Course_Item item;
            //foreach (Course_Item curItem in _user._termList[_user._termIndex]._courseList[_user._courseIndex]._items)
            //{
            //    if (curItem.Title == (sender as ListBoxItem)) { }
            //}

            #region Retreive existing Item data.

            Course_Item item = _user.ActiveCourse._items[0]; //TODO: Replace with actual active Item.
            string s_type = item.Type.ToUpper();
            string s_title = item.Title;
            string s_weight = $"{item.Weight * 100}%";
            string s_mark = item.Mark == -1 ? "" : (item.Grade * 100).ToString() + "%";
            DateTime? s_dueDate = (item.DueDate == DateTime.MaxValue) ? (DateTime?)null : item.DueDate;
            bool s_status = item.Status;   //TODO: Fix.

            #endregion

            #region Generate popup components.

            // Component - Type
            var typeStack = CreateStackPanel();
            typeStack.Children.Add(CreateTextBlock(text: "TYPE:"));
            typeStack.Children.Add(CreateTextBox(text: $"{s_type}", placeHolder: $"{s_type}"));

            // Component - Title
            var titleStack = CreateStackPanel();
            titleStack.Children.Add(CreateTextBlock(text: "TITLE:"));
            titleStack.Children.Add(CreateTextBox(text: $"{s_title}", placeHolder: $"{s_title}"));

            // Component - Weight
            var weightStack = CreateStackPanel();
            weightStack.Children.Add(CreateTextBlock(text: "WEIGHT(%):"));
            weightStack.Children.Add(CreateTextBox(text: $"{s_weight}", placeHolder: $"{s_weight}"));

            // Component - Mark
            var markStack = CreateStackPanel();
            markStack.Children.Add(CreateTextBlock(text: "MARK(%):"));
            markStack.Children.Add(CreateTextBox(text: $"{s_mark}", placeHolder: $"{s_mark}"));

            // Component - Duedate
            var stackDate = CreateStackPanel();
            var pickDate = new CalendarDatePicker
            {
                Width = 200,
                FontSize = 30,
                Foreground = new SolidColorBrush(Colors.Gray),
                Date = s_dueDate
            };
            _picker_dueDate = pickDate;
            var dueDate_Button = new Button
            {
                Content = "Clear",
                FontSize = 16,
                Width = 100,
                Foreground = new SolidColorBrush(Colors.Red),
                Background = new SolidColorBrush(Colors.White)
            };
            dueDate_Button.Tapped += new TappedEventHandler(OnClick_ClearDate);
            stackDate.Children.Add(CreateTextBlock("DUEDATE:"));
            stackDate.Children.Add(pickDate);
            stackDate.Children.Add(dueDate_Button);

            // Component - Status
            var finishedStack = CreateStackPanel();
            finishedStack.Children.Add(CreateTextBlock(text: "FINISHED:"));
            finishedStack.Children.Add(new CheckBox
            {
                VerticalAlignment = VerticalAlignment.Center,
                IsChecked = s_status,
                MinWidth = 300
            });

            // Append all components into stackPanel.
            var stack = new StackPanel { Orientation = Orientation.Vertical };
            stack.Children.Add(typeStack);
            stack.Children.Add(titleStack);
            stack.Children.Add(weightStack);
            stack.Children.Add(markStack);
            stack.Children.Add(stackDate);
            stack.Children.Add(finishedStack);

            // Generate ContentDialog window.
            var contentDialog = new ContentDialog
            {
                Title = "EDIT ITEM",
                Content = stack,
                PrimaryButtonText = "Confirm",
                CloseButtonText = "Cancel",
                DefaultButton = ContentDialogButton.Primary
            };

            #endregion

            // Show popup window.
            if (await contentDialog.ShowAsync() == ContentDialogResult.Primary)
            {
                var panel = contentDialog.Content as StackPanel;
                string type = ((panel.Children[0] as StackPanel).Children[1] as TextBox).Text;
                string title = ((panel.Children[1] as StackPanel).Children[1] as TextBox).Text;
                double weight; Double.TryParse(((panel.Children[2] as StackPanel).Children[1] as TextBox).Text.Replace("%", ""), out weight);
                double mark; Double.TryParse(((panel.Children[3] as StackPanel).Children[1] as TextBox).Text.Replace("%", ""), out mark);
                bool status = (bool)((panel.Children[5] as StackPanel).Children[1] as CheckBox).IsChecked;

                // Process DueDate value.
                var dueDatePicker = ((panel.Children[4] as StackPanel).Children[1] as CalendarDatePicker).Date;
                DateTime? dueDate = (dueDatePicker == null) ? (DateTime?)null : ((DateTimeOffset)dueDatePicker).DateTime;

                // Update Item.
                item.Update(type, title, (weight / 100), (mark / 100), dueDate, status);

                // Display success message.
                Alert("Item edited!");
                Frame.Navigate(typeof(Semester_Marks), _user);
            }
        }

        #endregion
    }
}
