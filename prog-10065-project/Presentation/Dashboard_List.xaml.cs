﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

namespace prog_10065_project {
    public sealed partial class Dashboard_List : Page {

        #region Field variables

        Account _user;
        
        #endregion

        #region Constructor

        public Dashboard_List() {
            this.InitializeComponent();
        }

        #endregion

        #region Miscellaneous methods

        private void TermList_Selection_Changed(object sender, SelectionChangedEventArgs e) {}

        #endregion

        #region Navigational methods

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // Update page data.
            _user = e.Parameter as Account;

            //TODO: Revert if doesn't work.
            //if (_user._termIndex >= 0)
            if (_user.ActiveTerm != null) { 

                // Update term listbox.
                foreach (Course course in _user.ActiveTerm._courseList)
                {

                    // Create course details.
                    Ellipse circle = new Ellipse()
                    {
                        Fill = new SolidColorBrush(Colors.Red),
                        Stroke = new SolidColorBrush(Colors.Red),
                        StrokeThickness = 5,
                        Height = 50,
                        Width = 50,
                    };
                    TextBlock code = new TextBlock()
                    {
                        Text = $"{course.Code}",
                        FontSize = 36,
                        Margin = new Thickness(30, 0, 100, 0),
                        FontWeight = FontWeights.Bold
                    };
                    TextBlock credit = new TextBlock()
                    {
                        Text = $"[{course.Credit}]",
                        FontSize = 36,
                        Margin = new Thickness(0, 0, 100, 0),
                    };
                    TextBlock average = new TextBlock()
                    {
                        Text = $"{course.Average * 100}%",
                        FontSize = 36
                    };

                    // Create stackpanel, and append course details.
                    StackPanel panel = new StackPanel()
                    {
                        Orientation = Orientation.Horizontal,
                        Margin = new Thickness(10, 5, 0, 5)
                    };
                    panel.Children.Add(circle);
                    panel.Children.Add(code);
                    panel.Children.Add(credit);
                    panel.Children.Add(average);

                    // Create listbox item, and append stack panel.
                    ListBoxItem boxItem = new ListBoxItem();
                    boxItem.Content = panel;

                    _listBox_TermList.Items.Add(boxItem);
                }
            }

            base.OnNavigatedTo(e);
        }

        #endregion
    }
}
