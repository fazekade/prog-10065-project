﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace prog_10065_project {
    public sealed partial class MainPage : Page {
        #region Constructor

        public MainPage() {
            this.InitializeComponent();
            
            // Enable page cache.
            this.NavigationCacheMode = Windows.UI.Xaml.Navigation.NavigationCacheMode.Enabled;

            // Opens existing account file, or creates default files otherwise.
            //OpenAccount();

            Frame.Navigate(typeof(DashboardPage));
        }

        #endregion

        /// <summary>
        /// Returns TRUE if the parameter (as STRING) is null or filled only with whitespace; (as T) is null or default; FALSE otherwise.
        /// </summary>
        /// <typeparam name="T">The type of the data being validated.</typeparam>
        /// <param name="value">The data being validated.</param>
        /// <returns></returns>
        public static bool CheckNullOrWhitespace<T>(T value) {
            if (typeof(T) == typeof(string)) {
                return string.IsNullOrWhiteSpace(value as string);
            }

            return (value == null) || (value.Equals(default(T))); 
        }
    }
}
