﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace prog_10065_project {

    public sealed partial class TestPage : Page {

        #region Field Variables

        private ListBox _xamlCourseList;
        private ComboBox _xamlTermDropdown;
        private TextBox _xamlTxtBox;

        #endregion

        public TestPage() {
            this.InitializeComponent();
            _xamlCourseList = _courseList;
            _xamlTermDropdown = _termDropdown;
            _xamlTxtBox = _textBox;

            Button b2 = new Button();
            b2.Content = "New button";
            _test.DoubleTapped += new DoubleTappedEventHandler(OnDoubleTapped);
        }

        private void TermList_Selection_Changed(object sender, SelectionChangedEventArgs e) { }

        private void Term_Changed(object sender, SelectionChangedEventArgs e) {
            // Prevents selectionChanged triggering prior to initializing object.
            if (_xamlCourseList != null) {
                //_xamlCourseList.Items.Clear();
                StackPanel course = new StackPanel() {
                    
                };
                _xamlCourseList.Items.Add("Hello");
            }
        }



        private async void Read(object sender, RoutedEventArgs e) {
            StorageFolder storageFile = ApplicationData.Current.LocalFolder;
            //string storageName = "Assets/MyData/Temp.txt";
            //StorageFile sampleFile = await StorageFile.GetFileFromPathAsync(storageName);
            string fileName = (_xamlTermDropdown.SelectionBoxItem.ToString()) + ".txt";
            StorageFile sampleFile = await storageFile.GetFileAsync(fileName);

            var buffer = await Windows.Storage.FileIO.ReadBufferAsync(sampleFile);
            using (var dataReader = DataReader.FromBuffer(buffer)) {
                string text = dataReader.ReadString(buffer.Length);
                var messageDialog = new MessageDialog(text);
                await messageDialog.ShowAsync();
            }
        }
        private async void Write(object sender, RoutedEventArgs e) {
            StorageFolder storageFile = ApplicationData.Current.LocalFolder;

            string fileName = (_xamlTermDropdown.SelectionBoxItem.ToString()) + ".txt";

            StorageFile sampleFile = await storageFile.CreateFileAsync(fileName, 
                CreationCollisionOption.ReplaceExisting);

            string text = _xamlTxtBox.Text;
            await FileIO.WriteTextAsync(sampleFile, text);

            var messageDialog = new MessageDialog(fileName);
            await messageDialog.ShowAsync();
        }

        private void OnFocus_TextBox(object sender, RoutedEventArgs e) {

        }

        private void OnDoubleTap_EditItem(object sender, DoubleTappedRoutedEventArgs e)
        {

        }

        private async void OnDoubleTapped(object sender, RoutedEventArgs e)
        {
            var messageDialog = new MessageDialog("Double tapped");
            await messageDialog.ShowAsync();
        }
    }
}
