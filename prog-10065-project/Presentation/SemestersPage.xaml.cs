﻿using prog_10065_project;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Markup;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace prog_10065_project {
    internal enum Type
    {
        Exam = 1,
        Quizzes,
        Assignments,
        Labs
    }

    public sealed partial class SemestersPage : Page {

        #region Field variables

        private Account _user;
        private CalendarDatePicker _picker_dueDate;
        private ContentDialog _dialog_AddItem;

        #endregion

        #region Constructor

        public SemestersPage()
        {
            this.InitializeComponent();

        }

        #endregion

        #region Navigational methods

        protected override void OnNavigatedTo(NavigationEventArgs e) {
            // Get the user data.
            _user = e.Parameter as Account;

            // Reset ActiveCourse and ActiveItem.
            _user.ActiveCourse = null;
            _user.ActiveItem = null;

            #region Setup Course comboBox.

            // Update course combobox.
            foreach (Course course in _user.ActiveTerm._courseList)
            {
                _combo_Course.Items.Add($"{course.Code}");
            }

            // If no course is selected, default to first course.
            if (_user.ActiveCourse == null)
            {
                _user.ActiveCourse = _user.ActiveTerm._courseList.First();
            }


            // Set selected index of course combobox to course index. 
            _combo_Course.SelectedIndex = _user.ActiveTerm._courseList.IndexOf(_user.ActiveCourse);

            #endregion

            // Update the term TextBlock. 
            _txt_Term.Text = $"{_user.ActiveTerm}";

            // Disable sidebar Scheduler button.
            _boxItem_Scheduler.IsEnabled = false;
            _btn_Performance.IsEnabled = false;
            _btn_Calculate.IsEnabled = false;
            _btn_DeleteItem.IsEnabled = false;

            // Update the color scheme of the tabs.
            ColorTabs(_btn_Overview);

            base.OnNavigatedTo(e);
        }

        #endregion

        #region Sidebar methods

        private void _btnHamburger_Click(object sender, RoutedEventArgs e)
        {
            // Toggle the Dashboard sidebar visibility.
            _splitView.IsPaneOpen = !_splitView.IsPaneOpen;

            // Collapse timestamp visibility.
            _boxItem_Timestamp.Visibility = Visibility.Visible;
        }

        private void _listIconBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //
        }

        private void Dashboard_Btn_Clicked(object sender, TappedRoutedEventArgs e)
        {
            // Navigate to Semester primary page.
            this.Frame.Navigate(typeof(DashboardPage), _user);
        }

        private void OnClosing_Sidebar(SplitView sender, SplitViewPaneClosingEventArgs args)
        {
            // Collapse timestamp visibility.
            _boxItem_Timestamp.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Event handler method. Verifies that/how the user wants to logout; saving changes or not.
        /// Author: Devon Fazekas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Logout_Clicked(object sender, TappedRoutedEventArgs e)
        {
            // Create popup window object
            ContentDialog logout = new ContentDialog
            {
                // Title
                Title = "Logging out?",
                // Content
                Content = "Any unsaved data will be permanetly lost. Would you like to save your changes?",

                // Close Btn - Text
                CloseButtonText = "Cancel",
                // Save Btn - Text
                PrimaryButtonText = "Save",
                // Don't Save Btn - Text
                SecondaryButtonText = "Don't save",

                // Set default button
                DefaultButton = ContentDialogButton.Close
            };

            // Show button and await for user response
            ContentDialogResult result = await logout.ShowAsync();

            // Save changes & logout
            if (result == ContentDialogResult.Primary)
            {
                SaveUser();
                Frame.Navigate(typeof(LoginPage));

            }
            // Logout without saving
            else if (result == ContentDialogResult.Secondary)
            {
                //Todo: Finish functionality.
                this.Frame.Navigate(typeof(LoginPage));
            }
        }

        private async void SaveUser()
        {
            // Get the folder.
            StorageFolder folder = ApplicationData.Current.LocalFolder;

            // Create the file name.
            string fileName = $"data_{_user.UserName.ToLower()}.txt";

            // Create the file, or open if exists.
            StorageFile file = await folder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);

            // Create the content.
            List<string> lines = new List<string> { $"{_user.Password},{_user.UserName},{_user.FirstName},{_user.LastName},{_user.Email}" };

            // Save all the terms.
            foreach (Term term in _user._termList)
            {
                lines.Add($"term,{term.Year},{term.Semester}");

                // Save all the courses.
                foreach (Course course in term._courseList)
                {
                    lines.Add($"course,{course.Code},{course.Title},{course.Credit},{course.Status}");

                    // Save all the items.
                    foreach (Course_Item item in course._items)
                    {
                        lines.Add($"item,{item.Type},{item.Title},{item.Weight},{item.Mark},{item.StartDate},{item.DueDate}");
                    }
                }
            }

            await FileIO.WriteLinesAsync(file, lines);

            // Display success message.
            MessageDialog message = new MessageDialog("Changes saved!");
            await message.ShowAsync();
        }

        #endregion

        #region Header methods

        private void Course_Changed(object sender, RoutedEventArgs e)
        {
            // Validate onload from onChange.
            if (_user != null)
            {
                //TODO: Revert if doesn't work.
                //_user._courseIndex = _combo_Course.SelectedIndex;
                _user.ActiveCourse = _user.ActiveTerm._courseList[_combo_Course.SelectedIndex];

                // Refresh subpage. 
                ColorTabs(_btn_Overview);
                _frame.Navigate(typeof(Semester_Overview), _user);

            }
        }

        private void Add_Term_Clicked(object sender, RoutedEventArgs e)
        {
            //
        }

        private void OnClick_DeleteCourse()
        {
            // Create popup window object
            ContentDialog deleteCourse = new ContentDialog
            {
                Title = "Delete Course?",
                Content = "Deleting a Course will delete all Courses and Items associated with it. This cannot be undone. Are you sure?",
                CloseButtonText = "Cancel",
                PrimaryButtonText = "Delete",
                DefaultButton = ContentDialogButton.Close
            };

            // Show button and await for user response
            //ContentDialogResult result = await deleteTerm.ShowAsync();

            // Save changes & logout
            /*if (result == ContentDialogResult.Primary)
            {
                //TODO: Finish functionality.
            }
            // Logout without saving
            else if (result == ContentDialogResult.Secondary)
            {
                //TODO: Finish functionality.
            }*/
        }

        /// <summary>
        /// Clears the DueDate of the Add Item popup window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClick_ClearDate(object sender, RoutedEventArgs e)
        {
            _picker_dueDate.Date = null;
        }

        private async void OnClick_AddItem(object sender, RoutedEventArgs e) {
            StackPanel CreateStackPanel(string orientation = "Horizontal") {                
                return new StackPanel {
                    Orientation = orientation == "Horizontal" ? Orientation.Horizontal : Orientation.Vertical,     
                };
            }
            TextBlock CreateTextBlock(string text = "", double leftMargin = 0, double rightMargin = 0, string foreground = "Black", string fontWeight = "", double width = 130, double fontSize = 24) {
                // Convert foreground from string to color.
                var s_foreground = (Color)XamlBindingHelper.ConvertValue(typeof(Color), foreground);

                // Convert fontWeight from string to fontWeight. 
                var s_fontWeight = fontWeight == "Bold" ? FontWeights.Bold : FontWeights.Normal;

                // Create and return TextBlock.
                return new TextBlock {
                    Text = $"{text}",
                    FontSize = fontSize,
                    Margin = new Thickness(leftMargin, 0, rightMargin, 0),
                    Foreground = new SolidColorBrush(s_foreground),
                    FontWeight = s_fontWeight,
                    Width = width,
                    VerticalAlignment = VerticalAlignment.Center
                };
            }
            TextBox CreateTextBox(string text = "", double leftMargin = 0, double rightMargin = 0, string foreground = "Gray", string fontWeight = "", double width = 300, double fontSize = 24, string placeHolder = "") {
                // Convert foreground from string to color.
                var s_foreground = (Color)XamlBindingHelper.ConvertValue(typeof(Color), foreground);

                // Convert fontWeight from string to fontWeight. 
                var s_fontWeight = fontWeight == "Bold" ? FontWeights.Bold : FontWeights.Normal;

                // Create and return TextBox.
                var box =  new TextBox {
                    Text = $"{text}",
                    PlaceholderText = $"{placeHolder}",
                    FontSize = fontSize,
                    Margin = new Thickness(leftMargin, 0, rightMargin, 0),
                    Foreground = new SolidColorBrush(s_foreground),
                    FontWeight = s_fontWeight,
                    Width = width,
                    VerticalAlignment = VerticalAlignment.Center
                };
                box.TextChanged += OnDataChanged_AddItem;

                return box;
            }


            #region Generate window components.
            
            // Component - Type.
            var stackType = CreateStackPanel();
            stackType.Children.Add(CreateTextBlock(text: "TYPE:"));
            stackType.Children.Add(CreateTextBox(placeHolder: "Quiz"));

            // Component - Title.
            var stackTitle = CreateStackPanel();
            stackTitle.Children.Add(CreateTextBlock(text: "TITLE:"));
            stackTitle.Children.Add(CreateTextBox(placeHolder: "#1"));

            // Component - Weight.
            var stackWeight = CreateStackPanel();
            stackWeight.Children.Add(CreateTextBlock(text: "WEIGHT(%):"));
            stackWeight.Children.Add(CreateTextBox(placeHolder: "5.40%"));

            // Component - DueDate.
            var stackDate = CreateStackPanel();
            var pickDate = new CalendarDatePicker {
                Width = 200,
                FontSize = 30,
                Foreground = new SolidColorBrush(Colors.Gray)
            };
            _picker_dueDate = pickDate;
            var dueDate_Button = new Button {
                Content = "Clear",
                FontSize = 16,
                Width = 100,
                Foreground = new SolidColorBrush(Colors.Red),
                Background = new SolidColorBrush(Colors.White)
            };
            dueDate_Button.Tapped += new TappedEventHandler(OnClick_ClearDate); 
            stackDate.Children.Add(CreateTextBlock("DUEDATE:"));
            stackDate.Children.Add(pickDate);
            stackDate.Children.Add(dueDate_Button);

            // Append all components into stackPanel.
            var stackPanel = CreateStackPanel("Vertical");
            stackPanel.Children.Add(stackType);
            stackPanel.Children.Add(stackTitle);
            stackPanel.Children.Add(stackWeight);
            stackPanel.Children.Add(stackDate);

            // Generate ContentDialog window.
            _dialog_AddItem = new ContentDialog
            {
                Title = "Add Item",
                Content = stackPanel,
                Width = 500,
                PrimaryButtonText = "Confirm",
                CloseButtonText = "Cancel",
                DefaultButton = ContentDialogButton.Primary,
                IsPrimaryButtonEnabled = false
            };
            //_dialog_AddItem.Closing += OnClosing_AddItem;

            #endregion

            // Show popup window.
            if (await _dialog_AddItem.ShowAsync() == ContentDialogResult.Primary) {
                var panel        = _dialog_AddItem.Content as StackPanel;
                string type      = ((panel.Children[0] as StackPanel).Children[1] as TextBox).Text;
                string title     = ((panel.Children[1] as StackPanel).Children[1] as TextBox).Text;
                double weight;   Double.TryParse(((panel.Children[2] as StackPanel).Children[1] as TextBox).Text.Replace("%", ""), out weight);

                // Process DueDate value.
                var dueDatePicker = ((panel.Children[3] as StackPanel).Children[1] as CalendarDatePicker).Date;
                var dueDate       = dueDatePicker == null ? DateTime.MaxValue : ((DateTimeOffset)dueDatePicker).DateTime;

                // Create new item.
                //TODO: Fix StartDate.
                Course_Item item = new Course_Item(type, title, (weight / 100), dueDate, DateTime.MaxValue);

                // Append item to list of items.
                _user.ActiveCourse._items.Add(item);

                // Display success message. Redirect to Marks subpage.
                Alert("Item added!");
                ColorTabs(_btn_Marks);
                _frame.Navigate(typeof(Semester_Marks), _user);
            }
        }

        /// <summary>
        /// Enables the AddItem ContentDialog confirm button by validating that all required fields contain valid data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDataChanged_AddItem(object sender, TextChangedEventArgs e) {
            StackPanel panel  = _dialog_AddItem.Content as StackPanel;
            string     type   = ((panel.Children[0] as StackPanel).Children[1] as TextBox).Text;
            string     title  = ((panel.Children[1] as StackPanel).Children[1] as TextBox).Text;
            string     weight = ((panel.Children[2] as StackPanel).Children[1] as TextBox).Text.Replace("%", "");   // Convert (4.5%) to (4.5).

            // Validate that values are neither whitespace nor null, and that Weight is numeric.
            if (MainPage.CheckNullOrWhitespace(type) || MainPage.CheckNullOrWhitespace(title) || MainPage.CheckNullOrWhitespace(weight) || !Double.TryParse(weight, out Double temp)) {
                _dialog_AddItem.IsPrimaryButtonEnabled = false;
                return;
            }

            // All required fields are valid. 
            _dialog_AddItem.IsPrimaryButtonEnabled = true;
        }

        private void OnClick_Options(object sender, RoutedEventArgs e)
        {

        }

        private void OnClick_DeleteItem(object sender, RoutedEventArgs e)
        {

        }

        private async void Alert(string msg)
        {
            MessageDialog message = new MessageDialog(msg);
            await message.ShowAsync();
        }

        #endregion

        #region Subframe methods

        private void SubpageTab_Click(object sender, RoutedEventArgs e) {
            List<Button> buttons = new List<Button>{ _btn_Overview, _btn_Marks, _btn_Performance, _btn_Calculate };
            System.Type[] pages = { typeof(Semester_Overview), typeof(Semester_Marks), typeof(Semester_Performance), typeof(Semester_Calculate) };

            // Get sender object.
            Button activeBtn = sender as Button;

            // Update the color scheme of the tabs.
            ColorTabs(activeBtn);

            // Navigate subframe to appropriate subpage.
            int index = 0;
            foreach (Button btn in buttons) {
                if (activeBtn == btn) {
                    index = buttons.IndexOf(btn);
                    break;
                }
            }
            _frame.Navigate(pages[index], _user);
        }

        

        private void ColorTabs(Button activeBtn = null) {
            Button[] buttons = { _btn_Overview, _btn_Marks, _btn_Performance, _btn_Calculate };

            // Revert color of all tabs.
            foreach (Button btn in buttons)
            {
                btn.Background = new SolidColorBrush(Colors.DarkCyan);
                btn.Foreground = new SolidColorBrush(Colors.White);
            }
            if (activeBtn != null)
            {
                // Color active tab.
                activeBtn.Background = new SolidColorBrush(Colors.White);
                activeBtn.Foreground = new SolidColorBrush(Colors.DarkCyan);
            }
        }

        #endregion
    }
}
