﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace prog_10065_project {
    public sealed partial class Dashboard_Overview : Page {

        #region Field variables

        Account _user;

        #endregion

        #region Constructor

        public Dashboard_Overview() {
            this.InitializeComponent();
        }

        #endregion

        #region Navigational methods

        protected override void OnNavigatedTo(NavigationEventArgs e) {
            // Update page data.
            _user = e.Parameter as Account;

            //TODO: Calculate GPA
            //TODO: Current avg.
            _user.Refresh();
            //TODO: Total credits
            //TODO: Credits earned

            //_txtGPAValue.Text = $"{_user._termList[_user._termIndex]._courseList[0]._items[0].Weight}";

            base.OnNavigatedTo(e);
        }

        #endregion
    }
}
