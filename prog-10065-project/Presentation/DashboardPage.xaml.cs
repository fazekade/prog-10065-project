﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace prog_10065_project {
    /// <summary>
    /// A primary page that hosts global functionality throughout its subpages.
    /// </summary>
    public sealed partial class DashboardPage : Page {
        #region Field variables 

        private Account _user;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for Dashboard primary page.
        /// Author: Devon Fazekas
        /// </summary>
        public DashboardPage()
        {
            this.InitializeComponent();  
        }

        #endregion

        #region Sidebar methods

        /// <summary>
        /// Event Handler method. Toggles the visibility of the Dashboard sidebar.
        /// Author: Devon Fazekas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _btnHamburger_Click(object sender, RoutedEventArgs e)
        {
            // Toggle the Dashboard sidebar visibility.
            _splitView.IsPaneOpen = !_splitView.IsPaneOpen;

            // Collapse timestamp visibility.
            _boxItem_Timestamp.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Event handler method. Dashboard sidebar navigation.
        /// Author: Devon Fazekas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _listIconBox_SelectionChanged(object sender, SelectionChangedEventArgs e) { }

        /// <summary>
        /// Event handler method. Semester button.
        /// Author: Devon Fazekas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Semester_Btn_Clicked(object sender, TappedRoutedEventArgs e)
        {
            // Navigate to Semester primary page.
            this.Frame.Navigate(typeof(SemestersPage), _user);
        }

        /// <summary>
        /// Event handler method. Verifies that/how the user wants to logout; saving changes or not.
        /// Author: Devon Fazekas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Logout_Clicked(object sender, TappedRoutedEventArgs e)
        {
            // Create popup window object
            ContentDialog logout = new ContentDialog
            {
                // Title
                Title = "Logging out?",
                // Content
                Content = "Any unsaved data will be permanetly lost. Would you like to save your changes?",

                // Close Btn - Text
                CloseButtonText = "Cancel",
                // Save Btn - Text
                PrimaryButtonText = "Save",
                // Don't Save Btn - Text
                SecondaryButtonText = "Don't save",

                // Set default button
                DefaultButton = ContentDialogButton.Close
            };

            // Show button and await for user response
            ContentDialogResult result = await logout.ShowAsync();

            // Save changes & logout
            if (result == ContentDialogResult.Primary)
            {
                SaveUser();
                Frame.Navigate(typeof(LoginPage));

            }
            // Logout without saving
            else if (result == ContentDialogResult.Secondary)
            {
                //Todo: Finish functionality.
                this.Frame.Navigate(typeof(LoginPage));
            }
        }

        private async void SaveUser()
        {
            // Get the folder.
            StorageFolder folder = ApplicationData.Current.LocalFolder;

            // Create the file name.
            string fileName = $"data_{_user.UserName.ToLower()}.txt";

            // Create the file, or open if exists.
            StorageFile file = await folder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);

            // Create the content.
            List<string> lines = new List<string> { $"{_user.Password},{_user.UserName},{_user.FirstName},{_user.LastName},{_user.Email}" };

            // Save all the terms.
            foreach (Term term in _user._termList)
            {
                lines.Add($"term,{term.Year},{term.Semester}");

                // Save all the courses.
                foreach (Course course in term._courseList)
                {
                    lines.Add($"course,{course.Code},{course.Title},{course.Credit},{course.Status}");

                    // Save all the items.
                    foreach (Course_Item item in course._items)
                    {
                        lines.Add($"item,{item.Type},{item.Title},{item.Weight},{item.Mark},{item.StartDate},{item.DueDate}");
                    }
                }
            }

            await FileIO.WriteLinesAsync(file, lines);

            // Display success message.
            MessageDialog message = new MessageDialog("Changes saved!");
            await message.ShowAsync();
        }

        private void OnClosing_Sidebar(SplitView sender, SplitViewPaneClosingEventArgs args)
        {
            // Collapse timestamp visibility.
            _boxItem_Timestamp.Visibility = Visibility.Collapsed;
        }

        #endregion

        #region Subframe methods

        private void SubpageTab_Click(object sender, RoutedEventArgs e)
        {
            List<Button> buttons = new List<Button> { _btn_Overview, _btn_List, _btn_Completion };
            System.Type[] pages = { typeof(Dashboard_Overview), typeof(Dashboard_List), typeof(Dashboard_Completion) };

            // Get sender object.
            Button activeBtn = sender as Button;

            // Update the color scheme of the tabs.
            ColorTabs(activeBtn);

            // Navigate subframe to appropriate subpage.
            int index = 1;
            foreach (Button btn in buttons)
            {
                if (activeBtn == btn)
                {
                    index = buttons.IndexOf(btn);
                    break;
                }
            }
            _frame.Navigate(pages[index], _user);
        }

        private void ColorTabs(Button activeBtn)
        {
            Button[] buttons = { _btn_Overview, _btn_List, _btn_Completion };

            // Revert color of all tabs.
            foreach (Button btn in buttons)
            {
                btn.Background = new SolidColorBrush(Colors.DarkCyan);
                btn.Foreground = new SolidColorBrush(Colors.White);
            }

            // Color active tab.
            activeBtn.Background = new SolidColorBrush(Colors.White);
            activeBtn.Foreground = new SolidColorBrush(Colors.DarkCyan);
        }

        #endregion

        #region Popup methods

        private void Validate_AddCourseInput(string data, TextBox obj) {
            if (String.IsNullOrWhiteSpace(data))
            {
                obj.BorderBrush = new SolidColorBrush(Colors.Red);
                throw new Exception("Missing or invalid input");
            }
        }

        private async void Alert(string msg) {
            MessageDialog message = new MessageDialog(msg);
            await message.ShowAsync();
        }

        #endregion

        #region Header methods

        private void Term_Changed(object sender, SelectionChangedEventArgs e)
        {
            // Change index of viewing term.
            //TODO: Delete index.
            //_user._termIndex = _comboTerm.SelectedIndex;
            _user.ActiveTerm = _user._termList[_comboTerm.SelectedIndex];
            //_user.ActiveTerm; 

            // Refresh subframe.
            ColorTabs(_btn_List);
            _frame.Navigate(typeof(Dashboard_List), _user);
        }

        /// <summary>
        /// Event handler method. Reveals options for the Dashboard page (add/delete Terms). 
        /// Author: Devon Fazekas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Dashboard_Options_Clicked(object sender, RoutedEventArgs e) { }

        private async void OnClick_Add_Term(object sender, RoutedEventArgs e)
        {
            // Create popup window and components.
            var stackYear = new StackPanel
            {
                Orientation = Orientation.Horizontal
            };
            var blockYear = new TextBlock
            {
                Width = 150,
                FontSize = 30,
                Text = "Year:"
            };
            var boxYear = new DatePicker
            {
                Width = 250,
                FontSize = 30,
                DayVisible = false,
                MonthVisible = false
            };
            stackYear.Children.Add(blockYear);
            stackYear.Children.Add(boxYear);

            var stackSemester = new StackPanel
            {
                Orientation = Orientation.Horizontal
            };
            var blockSemester = new TextBlock
            {
                Width = 150,
                FontSize = 30,
                Text = "Title:"
            };
            var boxSemester = new ComboBox
            {
                Width = 250,
                FontSize = 30
            };
            boxSemester.Items.Add(new ComboBoxItem { Content = "Summer", IsSelected = true });
            boxSemester.Items.Add(new ComboBoxItem { Content = "Fall" });
            boxSemester.Items.Add(new ComboBoxItem { Content = "Winter" });
            stackSemester.Children.Add(blockSemester);
            stackSemester.Children.Add(boxSemester);

            var stackPanel = new StackPanel();
            stackPanel.Children.Add(stackYear);
            stackPanel.Children.Add(stackSemester);
            ContentDialog dialog = new ContentDialog
            {
                Title = "Add Term",
                Content = stackPanel,
                CloseButtonText = "Cancel",
                PrimaryButtonText = "Confirm",
                DefaultButton = ContentDialogButton.Close
            };

            // Show button and await for user response
            if (await dialog.ShowAsync() == ContentDialogResult.Primary)
            {
                var panel = dialog.Content as StackPanel;
                DateTime tempYear = ((panel.Children[0] as StackPanel).Children[1] as DatePicker).Date.Date;
                string tempSemester = ((panel.Children[1] as StackPanel).Children[1] as ComboBox).SelectionBoxItem.ToString();

                // Create term.
                Term newTerm = new Term(tempYear.Year, tempSemester);

                // Validate that term is unique.
                foreach (Term term in _user._termList)
                {
                    if (newTerm.ToString() == term.ToString())
                    {
                        Alert("Term already exists!");
                        return;
                    }
                }

                // Append new term to list of terms.
                _user._termList.Add(newTerm);

                // Display success message.
                Alert("Term added! Reloading");

                // Refresh page.
                Frame.Navigate(typeof(DashboardPage), _user);
            }
        }

        /// <summary>
        /// Event handler method. Verifies that the user wants to delete a Term.
        /// Author: Devon Fazekas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Delete_Term_Clicked(object sender, RoutedEventArgs e)
        {
            // Create popup window object
            ContentDialog deleteTerm = new ContentDialog
            {
                // Title
                Title = "Delete Term?",
                // Content
                Content = "Deleting a Term will delete all Courses and Items associated with it. This cannot be undone. Are you sure?",

                // Close Btn - Text
                CloseButtonText = "Cancel",
                // Save Btn - Text
                PrimaryButtonText = "Delete",

                // Set default button
                DefaultButton = ContentDialogButton.Close
            };

            // Show button and await for user response
            ContentDialogResult result = await deleteTerm.ShowAsync();

            // Confirm term deletion.
            if (result == ContentDialogResult.Primary)
            {
                //TODO: Revert if doesn't work.
                //_user._termList.RemoveAt(_user._termIndex);
                _user._termList.Remove(_user.ActiveTerm);
                Alert("Term deleted!");
                Frame.Navigate(typeof(DashboardPage), _user);
            }
        }

        private async void OnClick_Add_Course(object sender, RoutedEventArgs e)
        {
            // Create popup window and components.
            var stackCode = new StackPanel
            {
                Orientation = Orientation.Horizontal
            };
            var blockCode = new TextBlock
            {
                Width = 150,
                FontSize = 30,
                Text = "Code:"
            };
            var boxCode = new TextBox
            {
                Width = 250,
                FontSize = 30,
                PlaceholderText = "PROG 10065"
            };
            stackCode.Children.Add(blockCode);
            stackCode.Children.Add(boxCode);

            var stackTitle = new StackPanel
            {
                Orientation = Orientation.Horizontal
            };
            var blockTitle = new TextBlock
            {
                Width = 150,
                FontSize = 30,
                Text = "Title:"
            };
            var boxTitle = new TextBox
            {
                Width = 250,
                FontSize = 30,
                PlaceholderText = "#1"
            };
            stackTitle.Children.Add(blockTitle);
            stackTitle.Children.Add(boxTitle);

            var stackCredit = new StackPanel
            {
                Orientation = Orientation.Horizontal
            };
            var blockCredit = new TextBlock
            {
                Width = 150,
                FontSize = 30,
                Text = "Credit:"
            };
            var boxCredit = new TextBox
            {
                Width = 250,
                FontSize = 30,
                PlaceholderText = "6.00"
            };
            stackCredit.Children.Add(blockCredit);
            stackCredit.Children.Add(boxCredit);

            var stackPanel = new StackPanel();
            stackPanel.Children.Add(stackCode);
            stackPanel.Children.Add(stackTitle);
            stackPanel.Children.Add(stackCredit);

            ContentDialog dialog = new ContentDialog
            {
                Title = "Add Course",
                Content = stackPanel,
                CloseButtonText = "Cancel",
                PrimaryButtonText = "Confirm",
                DefaultButton = ContentDialogButton.Close
            };

            // Show button and await for user response
            if (await dialog.ShowAsync() == ContentDialogResult.Primary)
            {
                var panel          = dialog.Content as StackPanel;
                string code        = ((panel.Children[0] as StackPanel).Children[1] as TextBox).Text;
                string title       = ((panel.Children[1] as StackPanel).Children[1] as TextBox).Text;
                double tempCredit; Double.TryParse(((panel.Children[2] as StackPanel).Children[1] as TextBox).Text, out tempCredit);

                // Validate input.
                try
                {
                    ValidateEmptyInput(code);
                    ValidateEmptyInput(title);
                }
                catch (Exception)
                {
                    Alert("One or more fields were empty. Action cancelled!");
                    return;
                }

                // Validate that course code is unique to chosen term.
                foreach (Course crs in _user.ActiveTerm._courseList)
                {
                    if (code == crs.Code)
                    {
                        // Display error in popup.
                        Alert("Course already exists!");

                        // Hault action.
                        return;
                    }
                }

                // Create course.
                Course course = new Course(code, title, tempCredit);

                // Append course to list of courses.
                _user.ActiveTerm._courseList.Add(course);

                // Alert success message.
                Alert("Course added! Reloading");

                // Refresh page.
                _boxItem_Semester.IsEnabled = true;
                Frame.Navigate(typeof(DashboardPage), _user);
            }
        }

        private void ValidateEmptyInput(string data)
        {
            if (String.IsNullOrWhiteSpace(data))
            {
                throw new Exception();
            }
        }

        #endregion

        #region Navigation methods

        protected override void OnNavigatedTo(NavigationEventArgs e) {
            // Update page data.
            _user = e.Parameter as Account;

            // Reset active properties.
            _user.ActiveTerm = null;
            _user.ActiveCourse = null;
            _user.ActiveItem = null;

            // If terms exist.
            if (_user._termList.Count > 0) {
                // Update term combobox.
                foreach (Term term in _user._termList) {
                    _comboTerm.Items.Add($"{term}");
                }

                // If no term is selected, select the first by default.
                //TODO: Revert if doesn't work.
                //if (_user._termIndex < 0) {
                if (_user.ActiveTerm == null) {
                    //_user._termIndex = 0;
                    _user.ActiveTerm = _user._termList.First();
                }

                // Set selected index of term combobox to term index. 
                //TODO: Revert if doesn't work.
                //_comboTerm.SelectedIndex = _user._termIndex;
                _comboTerm.SelectedIndex =  _user._termList.IndexOf(_user.ActiveTerm);

                // If no courses exist, disable sidebar features.
                if (_user.ActiveTerm._courseList.Count == 0) {
                    // Disable sidebar options.
                    _boxItem_Semester.IsEnabled = false;

                    // Prompt the user with actions.
                    _btn_DashboardOptions.Background = new SolidColorBrush(Colors.Red);
                    _btn_DashboardOptions.Foreground = new SolidColorBrush(Colors.White);
                    _btn_AddCourse.Background = new SolidColorBrush(Colors.Red);
                    _btn_AddCourse.Foreground = new SolidColorBrush(Colors.White);

                    // Update color scheme of tabs.
                    ColorTabs(_btn_Overview);
                    _frame.Navigate(typeof(Dashboard_Overview), _user);
                } else
                {
                    // Update color scheme of tabs.
                    ColorTabs(_btn_List);
                    _frame.Navigate(typeof(Dashboard_List), _user);
                }

            }
            // If terms don't exist, disable features.
            else
            {
                //TODO: Revert if doesn't work.
                //_user._termIndex   = -1;
                _user.ActiveTerm = null;
                //_user._courseIndex = -1;
                _user.ActiveCourse = null;
                _user.ActiveItem = null;

                // Disable subpage frames.
                _btn_Overview.IsEnabled     = false;
                _btn_List.IsEnabled         = false;
                _btn_Completion.IsEnabled   = false;

                // Disable Dashboard options.
                _btn_DeleteTerm.IsEnabled   = false;
                _btn_AddCourse.IsEnabled    = false;
                _comboTerm.IsEnabled        = false;

                // Disable sidebar options.
                _boxItem_Semester.IsEnabled = false;

                // Prompt the user with actions.
                _btn_DashboardOptions.Background = new SolidColorBrush(Colors.Red);
                _btn_DashboardOptions.Foreground = new SolidColorBrush(Colors.White);
                _btn_AddTerm.Background = new SolidColorBrush(Colors.Red);
                _btn_AddTerm.Foreground = new SolidColorBrush(Colors.White);
            } 

            // Disable sidebar Schedular button.
            _boxItem_Scheduler.IsEnabled = false;
            _btn_Completion.IsEnabled = false;

            base.OnNavigatedTo(e);
        }

        #endregion
    }
}
