﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace prog_10065_project {
    public sealed partial class Semester_Overview : Page {
        #region Field variables

        Account _user;

        #endregion

        #region Constructor 

        public Semester_Overview()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Navigational methods

        protected override void OnNavigatedTo(NavigationEventArgs e) {
            _user = e.Parameter as Account;

            //TODO: Calculate GPA
            //TODO: Current avg.
            //_user.Refresh();
            //TODO: Total credits
            //TODO: Credits earned

            //_txtCurAvgValue.Text = $"{_user._termList[_user._termIndex]._courseList[0]._items[0].Weight}";
            double gpa = 0;
            double average = 0;
            double credits = 0;
            double allCredits = 0;

            //foreach (Course_Item item in _user._termList[_user._termIndex]._courseList[_user._courseIndex]._items)
            //{
            //}
            _txt_Averege.Text = $"{_user.ActiveCourse.Average * 100}%";
            _txt_AchievedGrade.Text = $"{_user.ActiveCourse.AchievedGrade * 100}%";
            base.OnNavigatedTo(e);
        }
        
        #endregion
    }
}
