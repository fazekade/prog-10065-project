﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace prog_10065_project {
    public sealed partial class LoginPage : Page {
        #region Field variables

        private Button _xaml_Btn_Login;     // Login button.
        private Button _xaml_Btn_SignUp;    // SignUp button.

        #endregion

        #region Contructor

        public LoginPage() {
            this.InitializeComponent();
        }

        #endregion

        #region OnClick Methods

        /// <summary>
        /// OnClick event handler method. Changes the screen to display all required fields for Logging into an account.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Onclick_Btn_Login(object sender, RoutedEventArgs e)
        {
            // Save previous buttons.
            SaveButtons();

            // Create textboxes.
            CreateTextBox("Username");
            CreateTextBox("Password");

            // Create buttons.
            CreateButton("Confirm", "Login");
            CreateButton("Cancel");
        }

        /// <summary>
        /// OnClick event handler method. Changes the screen to display all required fields for Signing Up an account.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Onclick_Btn_SignUp(object sender, RoutedEventArgs e)
        {
            // Save previous buttons.
            SaveButtons();

            // Create textboxes.
            CreateTextBox("Username");
            CreateTextBox("First Name");
            CreateTextBox("Last Name");
            CreateTextBox("Email");
            CreateTextBox("Password");
            CreateTextBox("Confirm Password");

            // Create buttons.
            CreateButton("Confirm", "SignUp");
            CreateButton("Cancel");
        }

        /// <summary>
        /// OnClick event handler method. Processes the data from Login or Signup fields, differentiating between the two. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Onclick_Btn_Submit(object sender, RoutedEventArgs e)
        {
            Button submit = sender as Button; 

            // If Login submission.
            if (submit.Name == "_Btn_Login")
            {
                // Get Login details.
                TextBox txtboxUserName = (_Stack_Display.Children[0] as TextBox);
                TextBox txtboxPassword = _Stack_Display.Children[1] as TextBox;

                // Validate that all required fields are filled.
                string userName = txtboxUserName.Text;
                string password = txtboxPassword.Text;

                try
                {
                    ValidateInput(userName, txtboxUserName);
                    ValidateInput(password, txtboxPassword);
                }
                catch (Exception)
                {
                    // Display error in popup.
                    MessageDialog message = new MessageDialog("One or more fields are missing!");
                    await message.ShowAsync();

                    // Hault action.
                    return;
                }

                // Open account if exists.
                OpenAccount(userName, password);
            }
            // Else, Signup submission.
            else
            {
                // Get SignUp details.
                //FOR DUmmy
                TextBox userName = _Stack_Display.Children[0] as TextBox;
                TextBox firstName = _Stack_Display.Children[1] as TextBox;
                TextBox lastName = _Stack_Display.Children[2] as TextBox;
                TextBox email = _Stack_Display.Children[3] as TextBox;
                TextBox password = _Stack_Display.Children[4] as TextBox;
                TextBox password2 = _Stack_Display.Children[5] as TextBox;

                // Validate that all required fields are filled.
                try
                {
                    ValidateInput(userName.Text, userName);
                    ValidateInput(firstName.Text, firstName);
                    ValidateInput(lastName.Text, lastName);
                    ValidateInput(email.Text, email);
                    ValidateInput(password.Text, password);
                    ValidateInput(password2.Text, password2);
                }
                catch (Exception)
                {
                    // Display error in popup.
                    MessageDialog message = new MessageDialog("One or more fields are missing!");
                    await message.ShowAsync();

                    // Hault action.
                    return;
                }

                // Validate that passwords match.
                try
                {
                    if (password.Text != password2.Text)
                    {
                        throw new Exception();
                    }
                }
                catch (Exception)
                {
                    // Display error in popup.
                    MessageDialog message = new MessageDialog("The passwords entered do not match. Please try again!");
                    await message.ShowAsync();

                    // Hault action.
                    return;
                }

                // Create account.
                CreateAccount(userName.Text, firstName.Text, lastName.Text, email.Text, password.Text);

                // Load account.
                Account user = new Account(password.Text, userName.Text, firstName.Text, lastName.Text, email.Text);

                // Navigate to Dashboard page with User account.
                Frame.Navigate(typeof(DashboardPage), user);
            }
        }

        /// <summary>
        /// OnClick event handler method. Reverts the screen back to the starting page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Onclick_Btn_Cancel(object sender, RoutedEventArgs e)
        {
            // Clear screen.
            _Stack_Display.Children.Clear();

            // Restore Login/SignUp buttons.
            _Stack_Display.Children.Add(_xaml_Btn_Login);
            _Stack_Display.Children.Add(_xaml_Btn_SignUp);
        }

        #endregion

        #region UI Changing Methods

        //TODO: Write documentation.
        private void AddChild(object obj)
        {
            //TODO: Use Try-Catch to validate object type.
            Button btn = obj as Button;
            if (btn != null)
            {
                _Stack_Display.Children.Add(btn);
            }
            else
            {
                TextBox txtBox = obj as TextBox;
                if (txtBox != null)
                {
                    _Stack_Display.Children.Add(txtBox);
                }
            }
        }

        //TODO: Write documentation.
        private void CreateTextBox(string placeHolderText)
        {
            // Create textbox.
            TextBox txtBox = new TextBox
            {
                PlaceholderText = placeHolderText,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Margin = new Thickness(0, 0, 0, 30),
            };

            // Append textbox.
            AddChild(txtBox);
        }

        //TODO: Write documentation.
        private void CreateButton(string text, string ID = "")
        {
            // Create button.
            Button btn = new Button
            {
                Content = text,
                Name = "_Btn_" + text,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Margin = new Thickness(0, 0, 0, 30),
            };

            // Append event handler to button.
            if (btn.Name.ToString() == "_Btn_Confirm")
            {
                btn.Name = "_Btn_" + ID;
                btn.Click += new RoutedEventHandler(Onclick_Btn_Submit);
            }
            else
            {
                btn.Click += new RoutedEventHandler(Onclick_Btn_Cancel);
            }

            // Append button.
            AddChild(btn);
        }

        //TODO: Write documentation.
        private void SaveButtons()
        {
            // Save buttons.
            _xaml_Btn_Login = _Btn_Login;
            _xaml_Btn_SignUp = _Btn_SignUp;

            // Clear screen.
            _Stack_Display.Children.Clear();
        }

        #endregion

        #region Data Handling Methods

        /// <summary>
        /// Throws an exception if Textbox is either empty or filled with whitespace, and changes the Textbox border to red.
        /// </summary>
        /// <param name="input">The string content of Textbox.</param>
        /// <param name="box">The XAML Textbox element.</param>
        private void ValidateInput(string input, TextBox box) {
            if (String.IsNullOrWhiteSpace(input)) {
                box.BorderBrush = new SolidColorBrush(Colors.Red);
                throw new Exception("Missing or invalid input");
            }
        }

        /// <summary>
        /// Searches for and attempts to open the account from the Login data entered. 
        /// </summary>
        /// <param name="enteredUserName">Username</param>
        /// <param name="enteredPassword">Password</param>
        private async void OpenAccount(string enteredUserName, string enteredPassword) {
            // Find directory path.
            StorageFolder folder = ApplicationData.Current.LocalFolder;

            // File name.
            string fileName = $"data_{enteredUserName.ToLower()}.txt";

            // Attempt to find file.
            if (await folder.TryGetItemAsync(fileName) != null) {
                // Open file.
                StorageFile accountFile = await folder.GetFileAsync(fileName);

                // Read user details from account file.
                Account user;
                var buffer = await FileIO.ReadBufferAsync(accountFile);
                //string[] temp = (await FileIO.ReadTextAsync(accountFile)).Split('\n');
                using (var dataReader = DataReader.FromBuffer(buffer)) {
                    // Get all lines from file.
                    string[] lines = dataReader.ReadString(buffer.Length).Split('\n');

                    // Split header from lines.
                    string[] header = lines[0].Replace('\r', ' ').Trim().Split(',');

                    // Validate entered password matches saved password.
                    if (enteredPassword != header[0]) {
                        // Display error in popup.
                        var messageDialog = new MessageDialog("Either the username or password does not match existing files!");
                        await messageDialog.ShowAsync();

                        // Hault action.
                        return;
                    }

                    // Load user.
                    string password  = header[0];
                    string userName  = header[1];
                    string firstName = header[2];
                    string lastName  = header[3];
                    string email     = header[4];
                    user = new Account(password, userName, firstName, lastName, email);

                    // Load user data.
                    foreach (string data in lines.Skip(1)) {
                        string[] items = data.Replace('\r', ' ').Trim().Split(',');
                        LoadUserData(ref user, items);
                    }
                }

                // Redirect to DashboardPage.
                this.Frame.Navigate(typeof(DashboardPage), user);

            }
            else {
                // Display error in popup.
                MessageDialog message = new MessageDialog("Either the username or password does not match existing files!");
                await message.ShowAsync();

                // Hault action.
                return;
            }
        }

        /// <summary>
        /// Recreates the User object from the data found from the Account file.
        /// </summary>
        /// <param name="user">The user object.</param>
        /// <param name="data">Data from the Account file.</param>
        private void LoadUserData(ref Account user, string[] data) {
            // Term data.
            if (data.Length == 3) {
                int year        = Convert.ToInt32(data[1].Trim());
                string semester = data[2].Trim();

                // Add term to termList.
                Term term       = new Term(year, semester);
                user._termList.Add(term);
            }
                
            // Course data.
            else if (data.Length == 5) {
                string code   = data[1].Trim();
                string title  = data[2].Trim();
                double credit = Convert.ToDouble(data[3].Trim());
                bool status = Convert.ToBoolean(data[4].Trim());

                // Add course to courseList of last added term.
                Course course = new Course(code, title, credit, status);
                user._termList.Last()._courseList.Add(course);
            }
                
            // Item data.
            else if (data.Length == 7) {
                string type         = data[1].Trim();
                string title        = data[2].Trim();
                double weight       = String.IsNullOrWhiteSpace(data[3]) ? 0 : Convert.ToDouble(data[3].Trim());
                double mark         = String.IsNullOrWhiteSpace(data[4]) ? 0 : Convert.ToDouble(data[4].Trim());
                DateTime startDate; DateTime.TryParse(data[5].Trim(), out startDate);
                DateTime dueDate;   DateTime.TryParse(data[6].Trim(), out dueDate);

                // Add item to itemList of last added course and term.
                Course_Item item = new Course_Item(type, title, weight, dueDate, startDate, mark);
                user._termList.Last()._courseList.Last()._items.Add(item);
            }
        } 

        /// <summary>
        /// Attempts to create a new Account file from the data entered in the SignUp fields. 
        /// </summary>
        /// <param name="userName">Username.</param>
        /// <param name="firstName">First name.</param>
        /// <param name="lastName">Last name.</param>
        /// <param name="email">Email.</param>
        /// <param name="password">Password.</param>
        private async void CreateAccount(string userName, string firstName, string lastName, string email, string password) {
            // Get the folder.
            StorageFolder folder = ApplicationData.Current.LocalFolder;

            // Create the file name.
            string fileName = $"data_{userName.ToLower()}.txt";

            // Create the file, or open if exists.
            StorageFile file = await folder.CreateFileAsync(fileName, CreationCollisionOption.OpenIfExists);
            
            // Write user content to the file.
            string content = $"{password},{userName},{firstName},{lastName},{email}";
            await FileIO.WriteTextAsync(file, content);

            // Display success message.
            MessageDialog message = new MessageDialog("Created successfully!");
            await message.ShowAsync();
        }

        #endregion
    }
}
